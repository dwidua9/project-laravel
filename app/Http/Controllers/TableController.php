<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table(){
        return view('latihantable/table');
    }

    public function data(){
        return view('latihantable/data-tables');
    }
}
