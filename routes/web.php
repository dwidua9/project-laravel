<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/register', function () {
    return view('form');
});

Route::get('/welcome', function () {
    return view('home');
});

Route::get('/home', 'HomeController@home');

Route::get('/form', 'AuthController@form');

Route::get('/welcome', 'AuthController@welcome');

Route::get('/master', function () {
    return view('adminlte/master');
});

Route::get('/table', 'TableController@table');

Route::get('/data-tables', 'TableController@data');

//LatianCRUD

Route::get('/cast', 'CastController@index');

Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');

Route::get('/cast/{cast_id}', 'CastController@show');

Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::put('/cast/{cast_id}', 'CastController@update');

Route::delete('/cast/{cast_id}', 'CastController@destroy');
