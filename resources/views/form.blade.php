<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan HTML</title>
</head>
<body>
    <h1>Buat Accoutn Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="get">
        <label>First Name :</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="lastname"><br><br>

        <label>Gender :</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>

        <label>Nationality :</label><br><br>
        <select name="negara">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="thailand">Thailand</option>
            <option value="singapore">Singapore</option>
            <option value="vietnam">Vietnam</option>
        </select><br><br>

        <label>Language Spoken :</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>

        <label>Bio :</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>