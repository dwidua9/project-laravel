@extends('adminlte.master')

@section('content')

<div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                  @endif

                  @if(session('delete'))
                    <div class="alert alert-success">
                        {{ session('delete') }}
                    </div>
                  @endif

                  @if(session('update'))
                    <div class="alert alert-success">
                        {{ session('update') }}
                    </div>
                  @endif
                  <a class="btn btn-primary mb-2" href="/cast/create">Create New Cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($cast as $key=>$value)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $value->nama }}</td>
                            <td>{{ $value->umur }}</td>
                            <td>{{ $value->bio }}</td>
                            <td style="display: flex;">
                                <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <form action="/cast/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" align="center">No data</td>
                         </tr>  
                    @endforelse                      
                   </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>-->
            </div>
</div>
@endsection