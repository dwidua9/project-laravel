@extends('adminlte.master')

@section('content')

<div class="card">
    <div class="card-body">
        <h2>Show Cast {{$cast->id}}</h2>
        <p>{{$cast->nama}}</p>
        <p>{{$cast->umur}}</p>
        <p>{{$cast->bio}}</p>
    </div>    
</div>
@endsection